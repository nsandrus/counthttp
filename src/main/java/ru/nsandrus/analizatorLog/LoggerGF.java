package ru.nsandrus.analizatorLog;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class LoggerGF implements ReadLogger {

	//  need charset UTF8 or CP1251
	private static final String NEW_LINE = "\r\n";
	private static final String END_GF_LINE = "#]";
	private static final String BEGIN_GF_LINE = "[#";
	private static final int МАКСИМАЛЬНЫЙ_РАЗМЕР_СООБЩЕНИЯ = 1_000_000;
//	final static Logger LOG = Logger.getLogger(LoggerGF.class);
	private BufferedReader поток_Входной_Буферный;
	private long размер_Файла_Давно;
	private File file;
	private String имя_Файла;
	private FileInputStream fis;

	public LoggerGF(String filenameIn) throws FileNotFoundException {
		this.имя_Файла = filenameIn;
		file = new File(имя_Файла);
		fis = new FileInputStream(имя_Файла);
		поток_Входной_Буферный = new BufferedReader(new InputStreamReader(fis));
	}

	public LoggerGF(String filenameIn, String charsetName) throws FileNotFoundException, UnsupportedEncodingException {
		this.имя_Файла = filenameIn;
		file = new File(имя_Файла);
		fis = new FileInputStream(имя_Файла);
		поток_Входной_Буферный = new BufferedReader(new InputStreamReader(fis, charsetName));
	}

	@Override
	public String getMessage() throws IOException {
		String сообщение = поток_Входной_Буферный.readLine();
		if (сообщение == null) {
			return "";
		}

		if (сообщение.contains(BEGIN_GF_LINE) && !сообщение.contains(END_GF_LINE)) {
			сообщение = getGlassFishMessage(сообщение);
		}
		return сообщение;
	}

	private String getGlassFishMessage(String начало_Сообщения) throws IOException {
		StringBuilder сообщение = new StringBuilder(начало_Сообщения);
		checkRollingFile();
		while (поток_Входной_Буферный.ready()) {
			сообщение.append(NEW_LINE + поток_Входной_Буферный.readLine());
			if (сообщение.indexOf(END_GF_LINE) >= 0 ) {
				break;
			}
			if (сообщение.length() >= МАКСИМАЛЬНЫЙ_РАЗМЕР_СООБЩЕНИЯ) {
				System.err.println("MAX length");
				break;
			}
		}
		return сообщение.toString();
	}

	@Override
	public boolean hasNext() throws IOException {
		boolean правда_Готов_Читать = поток_Входной_Буферный.ready();
		if (!правда_Готов_Читать) {
			checkRollingFile();
		}
		return правда_Готов_Читать;
	}

	protected void checkRollingFile() throws IOException, FileNotFoundException {
		long размер_Файла_Сейчас = file.length();
		if (размер_Файла_Сейчас < размер_Файла_Давно) {
			System.out.println("Rolling file");
				sleep();
				long размер_Файла_Прямо_Сейчас = file.length();
					if (размер_Файла_Прямо_Сейчас < размер_Файла_Давно) {
						поток_Входной_Буферный.close();
						поток_Входной_Буферный = new BufferedReader(new FileReader(имя_Файла));
					}
		}
		размер_Файла_Давно = размер_Файла_Сейчас;
	}

	protected void sleep() {
		try {
			Thread.sleep(1400);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void close() throws IOException {
		поток_Входной_Буферный.close();
	}

	@Override
	public void goToEndFile() {
		int байт_Доступно;
		try {
			байт_Доступно = fis.available();
			fis.skip(байт_Доступно);
		} catch (IOException e1) {
		}
	}

}
