package ru.nsandrus.avs;

import ru.nsandrus.file.FileReader;

/*
 * Заглушка которая подсчитывает входящие запросы, которые прошли проверку
 */
public class Start {

	static String version = "0.0.6";

	public static void main(String[] args) {
		String config = null;

		System.out.println("Start Count Mock version " + version);
		if (args.length > 0) {
			System.out.println("Search config file");
			config = FileReader.readFile(args[0]);
		} else {
			System.err.println("Не указан файл конфига");
			return;
		}

		if (config == null) {
			System.out.println("Видимо файл не найден или пуст");
			return;
		}
		ParserAvs parser = new ParserAvs();
		PlanCountHttp plan = parser.getPlan(config);
		System.out.println("config to memory:\n" + config);

		plan.start();

		System.out.println("\nStop work!");
		plan.printReport();
		plan.saveReport();

	}

}
