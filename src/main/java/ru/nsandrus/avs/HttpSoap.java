package ru.nsandrus.avs;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;

public class HttpSoap {

	Logger LOG = Logger.getLogger(HttpSoap.class);

	public String postDataAuthorize(String urlstr, String parameters, String username, String password, int timeOut) {
		String defaultContentType = "text/xml;charset=UTF-8";
		return postDataAuthorize(urlstr, parameters, username, password, defaultContentType, timeOut);
	}


	public String postDataAuthorize(String urlstr, String parameters, String username, String password, String ContentType, int timeOut) {

		if (username == null) username= "";
		if (password == null) password= "";
		URL url;
		try {
			url = new URL(urlstr);
		} catch (MalformedURLException e1) {
			LOG.error("Error get new URL::" + e1.getMessage());
			System.err.println("HTTP POST::Error get URL link for send request");
			return "No URL is present to send request";
		}
		LOG.debug("START::URL=" + urlstr + "::login=" + username + "::pass=" + password);
		LOG.trace("START::parametres=" + parameters);
		HttpURLConnection hpcon = null;

		try {
			String userPassword = username + ":" + password;
			String encoding = DatatypeConverter.printBase64Binary(userPassword.getBytes());
			hpcon = (HttpURLConnection) url.openConnection();
			hpcon.setRequestMethod("POST");
			hpcon.setRequestProperty("Content-Length", "" + Integer.toString(parameters.getBytes().length));
			hpcon.setRequestProperty("Authorization", "Basic " + encoding);
			// hpcon.setRequestProperty("Content-Language", "en-US");
			hpcon.setRequestProperty("Content-Type", ContentType);
			hpcon.setRequestProperty("Connection", "Keep-Alive");
			hpcon.setRequestProperty("SOAPAction", "");
			hpcon.setUseCaches(false);
			// getting the response is required to force the request, otherwise it might not even be sent at all
			 hpcon.setReadTimeout(timeOut*1000);
			 hpcon.setConnectTimeout(timeOut*1000);
			hpcon.setDoInput(true);
			hpcon.setDoOutput(true);
			DataOutputStream printout = new DataOutputStream(hpcon.getOutputStream());
			LOG.debug("SEND::Data=" + parameters);
			printout.write(parameters.getBytes("UTF-8"));
			printout.flush();
			printout.close();
		} catch (IOException e) {
			System.err.println("Exception of write to HTTP::" + e.getMessage());

		}

		int codeResponse = 0;
		try {
			codeResponse = hpcon.getResponseCode();
			// System.out.println("get code response = " + hpcon.getResponseCode());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			LOG.error("Error connection to " + urlstr +"::" + e1.getMessage());
			return e1.getMessage();
		}

		BufferedReader in = null;

		if (codeResponse >= 400) {
			// читаем поток ошибки
			try {
				in = new BufferedReader(new InputStreamReader(hpcon.getErrorStream(), "UTF-8"));
			} catch (IOException e) {
				System.err.println("exeption read error stream ");
				return "";
			}
		} else {
			// все ок
			try {
				in = new BufferedReader(new InputStreamReader(hpcon.getInputStream(), "UTF-8"));
			} catch (IOException e) {
				System.err.println("exeption read good stream " + e.getMessage());
				return "";
			}
		}

		String input;
		StringBuilder response = new StringBuilder();

		try {
			while ((input = in.readLine()) != null) {
				response.append(input + "\r");
			}
		} catch (IOException e) {
			System.err.println("Ошибка чтения потока HTTP");
		}

		if (hpcon != null) {
			hpcon.disconnect();
		}

		return response.toString();
	}

	protected void sleep(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
