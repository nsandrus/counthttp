package ru.nsandrus.avs;

import java.io.IOException;
import java.util.ArrayList;

import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import ru.nsandrus.analizatorLog.AnalizatorFMQ;
import ru.nsandrus.mock.MockRunner;
import ru.nsandrus.report.Report;

public class PlanCountHttp {

	Logger LOG = Logger.getLogger(PlanCountHttp.class);
	public int workedTime;
	String fileConfigText = "";
	List<Mock> mocks;
	List<Thread> threads;
	public String mainPath;
	List<MockRunner> список_Обработчиков_Заглушек;
	Report report = new Report();
	StringBuilder errorMessage = new StringBuilder();
	public StringBuilder statistic = new StringBuilder();
	AnalizatorFMQ fmq;

	public ArrayList<String> getArrayPlan(String data) {
		String[] lines = data.split("\n");
		ArrayList<String> datas = new ArrayList<String>();
		for (String line : lines) {
			datas.add(line);
		}
		datas.add("");
		return datas;
	}

	/*
	 * запуск плана.
	 */
	public void start() {
		long startTime = System.currentTimeMillis();
		startMocks();
		sleep(1);

		System.out.println("Заглушка запущена. Для остановки нажмите <Enter>");

		// ожидать нажатия для выхода
		try {
			if (System.in.read() == -1) {
			}
		} catch (IOException e) {
			// TODO Auto-generated catch blockk
			e.printStackTrace();
		}

		stopMocks();
		long stopTime = System.currentTimeMillis();
		workedTime = (int) ((stopTime - startTime) / 1000L);
	}

	private void stopMocks() {
		LOG.info("Начало");
		Mock mockData = null;
		if (mocks == null) {
			return;
		}

		for (MockRunner исполнитель : список_Обработчиков_Заглушек) {

			исполнитель.завершить_Работу();
			sleep(1);
			mockData = исполнитель.получить_Данные();
			if (mockData != null) {
				statistic.append("Финальная статистика по заглушке " + mockData.name + "\n");
				for (Entry<String, Integer> line : mockData.mapJson.entrySet()) {
					statistic.append(line.getKey() + "\t" + line.getValue() + "\n");
				}
			}

		}

		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				String mes = "Ошибка при ожидании завершения заглушек" + e.getMessage();
				LOG.error(mes);
				System.err.println(mes);
			}
		}
		LOG.info("Конец");

	}

	private void startMocks() {
		LOG.info("Начало");
		if (mocks == null) {
			return;
		}
		список_Обработчиков_Заглушек = new ArrayList<MockRunner>();
		threads = new ArrayList<>();

		for (Mock заглушка : mocks) {
			заглушка.readResponseFromFile(mainPath);
			LOG.info("Запуск заглушки на порту=" + заглушка.port);
			MockRunner обработчик_Заглушки = new MockRunner(заглушка);
			Thread thread = new Thread(обработчик_Заглушки);
			thread.start();
			threads.add(thread);
			список_Обработчиков_Заглушек.add(обработчик_Заглушки);
		}

		LOG.info("Конец");
	}

	void getMainPath(String fileCfgAvs) {
		String[] lines = fileCfgAvs.split("\n");
		for (String line : lines) {
			if (line.trim().startsWith("PATH=")) {
				String path = line.trim().substring(5);
				LOG.debug("Path=" + path);
				if (!path.endsWith("/")) {
					path += "/";
				}
				LOG.info("Main PATH=" + path);
				mainPath = path;
				return;
			}
		}
		mainPath = "";
	}

	void setMocks(String textCfg) {
		// получили текст конфига, надо найти все заглушки и забить их в массив
		String[] lines = textCfg.split("\n");
		boolean flagMock = false;
		ConcurrentHashMap<String, Integer> mapJson = new ConcurrentHashMap<>();
		String nameMock = "";
		int portMock = 8080;
		int timeout = 5;
		int timeout2 = 0;
		String fileResponse = "";
		for (String line : lines) {
			String lineTrim = line.trim();
			if (isStart(lineTrim, "MOCK") && !flagMock) {
				if (mocks == null) {
					mocks = new ArrayList<>();
				}
				flagMock = true;
				nameMock = lineTrim.substring(5);
				continue;
			}
			if (flagMock) {
				if (isStart(lineTrim, "PORT=")) {
					LOG.debug("PORT=" + lineTrim.substring(5));
					portMock = Integer.parseInt(lineTrim.substring(5));
					continue;
				}

				if (isStart(lineTrim, "RESPONSE=")) {
					fileResponse = lineTrim.substring(10, lineTrim.length() - 1);
					continue;
				}
				if (isStart(lineTrim, "TIMEOUT=")) {
					LOG.debug("TIMEOUT=" + lineTrim.substring(8));
					String time = lineTrim.substring(8);
					if (time.contains("-")) {
						// System.out.println("Random timeout found");
						String[] times = time.split("-");
						timeout = Integer.parseInt(times[0]);
						timeout2 = Integer.parseInt(times[1]);
					} else {
						timeout = Integer.parseInt(lineTrim.substring(8));
					}
					continue;
				}
				if (!lineTrim.isEmpty()) {
					mapJson.put(lineTrim, 0);
				}

			}

			if (flagMock && lineTrim.isEmpty()) {
				flagMock = false;
				Mock mock = new Mock(nameMock, portMock, fileResponse, mapJson, timeout, timeout2);
				mocks.add(mock);
			}
		}
		LOG.debug("Mock=" + mocks);
	}

	private boolean isStart(String lineTrim, String command) {
		return lineTrim.toUpperCase().startsWith(command);
	}

	private void sleep(int i) {
//		System.out.print(".");
		try {
			Thread.sleep(i * 1000);
		} catch (InterruptedException e) {
		}

	}

	public void printReport() {
		report.вывести_Отчет(this);
	}

	public void saveReport() {
		report.вывести_Отчет_Файл(this);

	}

}
