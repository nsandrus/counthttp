package ru.nsandrus.avs;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import ru.nsandrus.file.FileReader;

public class Mock {

	Logger LOG = Logger.getLogger(Mock.class);

	@Override
	public String toString() {
		return "Mock [name=" + name + ", port=" + port + ", response=" + fileResponse + "]";
	}
	public String name;
	public int port;
	public int timeout;
	public int timeout2;
	public String fileResponse = "";
	public String response = "";
	public String responseAll = "";
	public String receive = "";
	public ConcurrentHashMap<String, Integer> mapJson;// = new ConcurrentHashMap<>();


	public Mock(String nameMock, int port2, String fileResponse, ConcurrentHashMap<String, Integer> mapJson , int timeout, int timeout2) {
		this.name = nameMock;
		this.port = port2;
		this.fileResponse = fileResponse;
		this.mapJson = mapJson;
		this.timeout = timeout;
		this.timeout2 = timeout2;
	}

	public void readResponseFromFile(String mainPath) {
			String textFile = FileReader.readFile(mainPath + fileResponse);
			responseAll = textFile.trim();
			responseMessage(responseAll);
	}

	private void responseMessage(String data) {
		LOG.debug("incoming data=" + data);
//		String[] lines = data.split("\n");


		int start = data.indexOf("XML=");
		if (start <0) {
			LOG.error("NOT FOUND SOAP MESSAGE::data=" + data);
			response = "NOT FOUND SOAP MESSAGE!";
			System.err.println("NOT FOUND XML= in Soap message!");
			return;
		}
		// может быть несколько овтетов в зависимости от URL входящего
		int endXml = data.indexOf("\nURL=", start+4);
		if (endXml < 0) {
		response = data.substring(start + 4);
		} else {
			response = data.substring(start + 4, endXml - 1);
		}
	}

	public void setResponse(String response2) {
		responseAll = response2.trim();
		responseMessage(responseAll);

	}

}
