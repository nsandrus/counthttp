package ru.nsandrus.avs;

import org.apache.log4j.Logger;

public class SoapMessage {

	Logger LOG = Logger.getLogger(SoapMessage.class);

	public String url;
	public String login;
	public String pass;
	public String message;
	public String response;


	public SoapMessage(String data) {
		LOG.debug("incoming data=" + data);
		String[] lines = data.split("\n");


		for (String line : lines) {
			if (line.toUpperCase().startsWith("URL=")) {
				url = line.trim().substring(4);
			}
			if (line.toUpperCase().startsWith("LOGIN=")) {
				login = line.trim().substring(6);
			}
			if (line.toUpperCase().startsWith("PASS=")) {
				pass = line.trim().substring(5);
			}



		}

		int start = data.indexOf("XML=");
		if (start <0) {
			LOG.error("NOT FOUND SOAP MESSAGE::data=" + data);
			message = "NOT FOUND SOAP MESSAGE!";
			System.err.println("NOT FOUND SOAP MESSAGE!");
		}
		message = data.substring(start + 4);
	}


	@Override
	public String toString() {
		return "SoapMessage [LOG=" + LOG + ", url=" + url + ", login=" + login + ", pass=" + pass + ", message=" + message + ", response=" + response + "]";
	}



}
