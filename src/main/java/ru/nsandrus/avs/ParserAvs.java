package ru.nsandrus.avs;

import java.util.ArrayList;
import org.apache.log4j.Logger;

public class ParserAvs {

	Logger LOG = Logger.getLogger(ParserAvs.class);

	/*
	 * Парсер должен парсит а не собирать объект PlanAvs
	 */
	public PlanCountHttp getPlan(String fileCfgAvs) {
		LOG.info("START::fileCfg=\n" + fileCfgAvs);
		PlanCountHttp plan = new PlanCountHttp();
		plan.setMocks(fileCfgAvs);
		plan.fileConfigText = fileCfgAvs;
		plan.getMainPath(fileCfgAvs);
	
		LOG.debug("PLAN=" + plan);
		return plan;
	}


	public ArrayList<String> getArrayPlan(String data) {
		String[] lines = data.split("\n");
		ArrayList<String> datas = new ArrayList<String>();
		for (String line : lines) {
			datas.add(line);
		}
		datas.add("");
		return datas;
	}

}
