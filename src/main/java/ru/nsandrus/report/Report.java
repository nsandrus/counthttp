package ru.nsandrus.report;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import ru.nsandrus.avs.PlanCountHttp;

public class Report {

	StringBuilder logReport = new StringBuilder();

	public void вывести_Отчет(PlanCountHttp план) {
		System.out.println("Время работы " + getWorkTime(план.workedTime));
		System.out.println(план.statistic.toString());

	};

	public void вывести_Отчет_Файл(PlanCountHttp план) {

		File file = createFile(план);

		PrintWriter out = null;
		try {
			out = new PrintWriter(file.getAbsoluteFile());

			out.print("Время работы " + getWorkTime(план.workedTime) +"\n\n");
			out.printf("%s\n", план.statistic.toString());
		} catch (FileNotFoundException e) {
			System.err.println("Ошибка записи отчета." + e.getMessage());
		} finally {
			out.close();
		}

	}

	protected File createFile(PlanCountHttp план) {
		Date curDate = new Date();
		SimpleDateFormat format = new SimpleDateFormat();
		format = new SimpleDateFormat("yyyy_MM_dd-HH_mm_ss");
		String DateToStr = format.format(curDate);

		File dir = new File("Reports");
		if (!dir.exists()) {
			dir.mkdir();
		}

		File file = new File("Reports/" + "Отчет_" + DateToStr + ".txt");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				System.err.println("Ошибка создания отчета." + e.getMessage());
			}
		}
		return file;
	}

	public void addReport(String report) {
		logReport.append(report);
	};


	private String getWorkTime(int workedTime) {
		StringBuilder result = new StringBuilder();


		    int hours = workedTime / 3600;
		    int minutes = (workedTime % 3600) / 60;
		    int seconds = workedTime % 60;
		    result.append(hours + "ч " + minutes + "мин " + seconds+ "сек ");


		return result.toString();
	}
}

