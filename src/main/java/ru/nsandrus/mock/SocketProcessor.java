package  ru.nsandrus.mock;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;



//import org.apache.log4j.Logger;

import ru.nsandrus.avs.Mock;

public class SocketProcessor implements Runnable {
	private Socket сокет;
	private InputStream поток_Чтения;
	private OutputStream поток_Записи;
	private Mock заглушка;
	private String inPath = "";
//	final static Logger LOG = Logger.getLogger(SocketProcessor.class);

	public SocketProcessor(Socket socket, Mock заглушка) {
		this.сокет = socket;
		this.заглушка = заглушка;
		try {
			this.поток_Чтения = сокет.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			this.поток_Записи = сокет.getOutputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		String тело_Запроса = чтение_Тела_Запроса().trim();
//		 System.out.println("запрос был на порт " + заглушка.port + " BODY=" + тело_Запроса);
//		LOG.debug("запрос был на порт " + заглушка.port + " BODY=" + тело_Запроса);
		String новыйОтвет = получить_Настроенный_Ответ(тело_Запроса);
		if (заглушка.mapJson.containsKey(тело_Запроса)) {
			int count = заглушка.mapJson.get(тело_Запроса);
			count++;
//			System.out.println("найдено совпадение["+тело_Запроса+"], счетчик=" + count);

			заглушка.mapJson.put(тело_Запроса, count);


		}
//		System.out.println("timeout=" + заглушка.timeout + "-" + заглушка.timeout2);
		заснуть(заглушка.timeout, заглушка.timeout2);  // тут будем регулировать время ответа
		послать_Ответ(новыйОтвет);
//		LOG.debug("будем закрывать все потоки");
		try {
			поток_Записи.close();
			поток_Чтения.close();
			сокет.close();
		} catch (Throwable t) {
			/* do nothing */
//			LOG.warn("какие-то проблемы при закрытии потока " + t.getMessage());
		}
	}

	protected void заснуть(int msec, int msec2) {
		int wait = 0;
		if (msec2 !=0 && msec2 > msec) {
			wait = (int) (msec + Math.random()*(msec2 - msec));
		} else {
			wait = msec;
		}
//		System.out.println("wait=" + wait);
		try {
			Thread.sleep(wait);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private String чтение_Тела_Запроса() {
		StringBuilder весь_Запрос = new StringBuilder();
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(поток_Чтения, "UTF-8"));
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}

		waitInputData(in);

		try {
			while (in.ready()) {
				char ch = (char) in.read();
				весь_Запрос.append(ch);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		// System.out.println("RAW запрос был на порт " + заглушка.port + "::=" + весь_Запрос);
		String[] paths = весь_Запрос.toString().split(" ");
		if (paths.length > 2) {
			inPath = paths[1];
//			LOG.debug("Incoming path url=" + inPath);
		}

		// заглушка может отвечать с пустой строки. тогда массива будет 3
		int start = весь_Запрос.indexOf("\n\r");
		if (start < 0) {
//			LOG.debug("Позиция пустой строки=" + start + " ::длина запроса=" + весь_Запрос.length());
			return "";
		}
//		LOG.debug("Index start=" + start);
		String body = весь_Запрос.substring(start + 1);
//		LOG.debug("Body=" + body);
		return body;
	}

	protected void waitInputData(BufferedReader in) {
		/*
		 * блок чтобы гарантированно считать тело запроса
		 */
		try {
			while (!in.ready()) {
				try {
					Thread.sleep(30);
				} catch (InterruptedException e1) {
				}
			}
		} catch (IOException e1) {
		}
	}

	private void послать_Ответ(String ответ) {
//		LOG.debug("MOCK RESPONSED=" + ответ);
		try {
			поток_Записи.write(ответ.getBytes("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			поток_Записи.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * тут надо обернуть в HTPP 200 OK к примеру или это прямо указывать в заглушке
	 */
	private String получить_Настроенный_Ответ(String входящийЗапрос) {
		заглушка.receive = входящийЗапрос;
		String[] responsesFile = заглушка.responseAll.split("URL=");
		for (String respInFile : responsesFile) {
			if (respInFile.trim().isEmpty()) {
				continue;
			}

			int end = respInFile.indexOf("\n");
			if (end < 0) {
				logError("error find URL in response mock ::end=" + end + "::len=" + respInFile.length());
				return "NOT FOUND BY RESPONSE OF MOCK";
			}

			String url = respInFile.substring(0, end - 1);

			if (inPath.toUpperCase().startsWith(url.toUpperCase())) {
				String xml = respInFile.substring(respInFile.indexOf("XML=") + 4);
				заглушка.response = xml;
				return заглушка.response;
			}
		}

		String mes = "HTTP/1.1 500 Internal Server Error\n\nNot found XML for this Request in MOCK on path=" + inPath;
		System.err.println(mes);
		return mes;
	}

	protected void logError(String mes) {
		System.err.println(mes);
//		LOG.error(mes);
	}
}
