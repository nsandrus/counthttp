package ru.nsandrus.mock;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map.Entry;

import ru.nsandrus.avs.Mock;

public class MockRunner extends Thread {

	private volatile boolean isAlive;
	private volatile Mock globalMock;
	private volatile int status = 0;

	public String getNameMock() {
		return globalMock.name;
	}

	public MockRunner(Mock mock) {
		globalMock = mock;
	}

	public Thread завершить_Работу() {
		isAlive = false;
		return this;
	}

	public void run() {
		status = 1;

		isAlive = true;
		Long текущее_Время = System.currentTimeMillis();
		String времяСтарта = millisToDate(текущее_Время);
		Long староеВремя = текущее_Время;
		Long началоВремя = текущее_Время;

		ServerSocket ss;
		try {
			ss = new ServerSocket(globalMock.port);
			ss.setSoTimeout(500);
		} catch (IOException e) {
			System.err.println("Error start Mock on port="+globalMock.port+"::name=" + globalMock.name + "::" + e.getMessage());
			return;
		}
		while (isAlive) {
			текущее_Время = System.currentTimeMillis();
			if ( (текущее_Время - староеВремя) >= 1000 ) {
				староеВремя = текущее_Время;
				cls();
				System.out.println("Старт ["+времяСтарта+"] \tТекущее время ["+ millisToDate(текущее_Время)+"] \tвремя работы = " + getWorkTime((текущее_Время - началоВремя) / 1000 ));
				for ( Entry<String, Integer> line :globalMock.mapJson.entrySet()) {
					System.out.println(line.getKey() + "\t" + line.getValue());
				}
				System.out.println("Для завершения работы нажмите <Enter>");
			}
			try {
				Socket socket = null;
				socket = ss.accept();
				new Thread(new SocketProcessor(socket, globalMock)).start();
			} catch (Throwable e1) {
			}
		}
		try {
			ss.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		status = 2;
	}

	protected String getWorkTime(Long s) {
		StringBuilder result = new StringBuilder();
		    Long hours = s / 3600;
		    Long minutes = (s % 3600) / 60;
		    Long seconds = s % 60;
		    result.append(hours + "ч " + minutes + "мин " + seconds+ "сек ");
		return result.toString();
	}

	public Mock получить_Данные() {
		if (status == 2) {
			return globalMock;
		} else {
			return null;
		}
	}

	    public void cls(String... arg) {
	        try {
				new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
			} catch (InterruptedException | IOException e) {
//				LOG.error("CLS глючит" + e.getMessage());
			}
	    }

	    private String millisToDate(long millis){
	        SimpleDateFormat sdfDate = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");//dd/MM/yyyy
	        Date now = new Date();
	        String strDate = sdfDate.format(now);
	        return strDate;
	    }
}
